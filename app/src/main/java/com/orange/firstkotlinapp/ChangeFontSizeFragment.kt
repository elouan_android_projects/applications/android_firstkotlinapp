package com.orange.firstkotlinapp

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.ImageButton
import com.orange.firstkotlinapp.extension.DEFAULT_TEXT_SIZE
import com.orange.firstkotlinapp.extension.bind
import com.orange.firstkotlinapp.extension.toDensityPixel
import android.view.inputmethod.EditorInfo
import com.orange.firstkotlinapp.extension.SEND_FONT_SIZE_KEY

private const val KEY_CURRENT_SIZE = "currentSize"


class ChangeFontSizeFragment : DialogFragment() {

    private val TAG = this::class.simpleName

    private val mIncreaseSizeButton by bind<ImageButton>(R.id.ChangeColor_button_decrease)
    private val mDecreaseSizeButton by bind<ImageButton>(R.id.ChangeColor_button_increase)
    private val mEditFontSize by bind<EditText>(R.id.ChangeColor_value)

    private var mCurrentFontSize = toDensityPixel(DEFAULT_TEXT_SIZE)
    private lateinit var mSizeUpdater: OnSizeUpdate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mCurrentFontSize = arguments!!.getFloat(SEND_FONT_SIZE_KEY)
        return inflater.inflate(R.layout.fragment_change_font_size, container, false)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mSizeUpdater = context as OnSizeUpdate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // If there is saved saved bundle load it
        savedInstanceState?.let { mCurrentFontSize = savedInstanceState.getFloat(KEY_CURRENT_SIZE) }
        updateSize(mCurrentFontSize)
        updateSizeDisplay()
        Log.d(TAG, "current font size : $mCurrentFontSize")
        super.onViewCreated(view, savedInstanceState)
        mIncreaseSizeButton.setOnClickListener { updateSize(mCurrentFontSize.inc()); updateSizeDisplay() }
        mDecreaseSizeButton.setOnClickListener { updateSize(mCurrentFontSize.dec()); updateSizeDisplay() }

        mEditFontSize.setOnEditorActionListener { v, actionId, event ->
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                || actionId == EditorInfo.IME_ACTION_DONE
                || event != null && event.action == KeyEvent.ACTION_DOWN && event.keyCode == KeyEvent.KEYCODE_ENTER
            ) {
                updateSizeDisplay()
            }
            false
        }

        mEditFontSize.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(text: Editable?) {
                updateSize(if (text.toString().isEmpty()) null else text.toString().toFloat())
            }

            override fun beforeTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
            }

            override fun onTextChanged(text: CharSequence?, start: Int, lengthBefore: Int, lengthAfter: Int) {
            }

        })
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putFloat(KEY_CURRENT_SIZE, mCurrentFontSize)
    }

    private fun updateSize(newValue: Float?) {
        mCurrentFontSize = when (newValue) {
            null -> mCurrentFontSize
            in 0.0..999.0 -> newValue
            else -> mCurrentFontSize
        }
        Log.d(TAG, "current font size : $mCurrentFontSize")
    }

    private fun updateSizeDisplay() {
        mEditFontSize.setText(mCurrentFontSize.toInt().toString())
        mEditFontSize.setSelection(mEditFontSize.length())
        mSizeUpdater.updateActivitySize(mCurrentFontSize)
    }

    interface OnSizeUpdate {
        fun updateActivitySize(newFontSize: Float)
    }


}