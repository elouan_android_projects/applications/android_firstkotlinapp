package com.orange.firstkotlinapp.extension

import android.graphics.Color

const val DEFAULT_TEXT_SIZE = 90
const val DEFAULT_TEXT_COLOR = "#F16E00"

const val SEND_FONT_SIZE_KEY = "FONT_SIZE_KEY"
const val SEND_FONT_COLOR_KEY = "COLOR_KEY"

val COLOR_LIST = listOf(
    Color.parseColor(DEFAULT_TEXT_COLOR),
    Color.RED,
    Color.BLUE,
    Color.GREEN,
    Color.YELLOW,
    Color.CYAN,
    Color.MAGENTA
)

val COLOR_NAME_LIST = listOf(
    "Orange",
    "Red",
    "Blue",
    "Green",
    "Yellow",
    "Cyan",
    "Magenta"
)