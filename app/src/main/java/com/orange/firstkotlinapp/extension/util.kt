package com.orange.firstkotlinapp.extension

import android.app.Activity
import android.content.res.Resources
import android.support.annotation.IdRes
import android.support.v4.app.Fragment
import android.view.View


/* FUNCTION TO SIMPLIFY findViewById */
// The first one is for activity the second one is for fragments
// the warning is suppressed because the crash can only came from wrong type cast
// return type Lazy to be able to use val
// Since bind will be called only by the main thread we don't need to be in ThreadSafe (default Lazy value)
fun <T : View> Activity.bind(@IdRes res: Int): Lazy<T> {
    @Suppress("UNCHECKED_CAST")
    return lazy(LazyThreadSafetyMode.NONE) { findViewById<T>(res) }
}

fun <T : View> Fragment.bind(@IdRes res: Int): Lazy<T> {
    @Suppress("UNCHECKED_CAST")
    return lazy(LazyThreadSafetyMode.NONE) { view!!.findViewById<T>(res) }
}

/* FUNCTION TO TRANSFORM dp INTO px */
fun toPixel(dp: Int) = dp * Resources.getSystem().displayMetrics.density

/* FUNCTION TO TRANSFORM px INTO dp */
fun toDensityPixel(px: Int) = px / Resources.getSystem().displayMetrics.density




