package com.orange.firstkotlinapp


import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.Toolbar
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.EditText

import com.orange.firstkotlinapp.extension.bind

const val SEND_TEXT = "com.orange.firstkotlinapp.SEND_TEXT"

class MainActivity : AppCompatActivity() {

    private val TAG = this::class.simpleName

    private val mToolbar by bind<Toolbar>(R.id.toolbar)
    private val mEditText by bind<EditText>(R.id.MainActivity_editText)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        setSupportActionBar(mToolbar)


    }

    fun sendText(view: View) {
        val message = mEditText.text.toString()

        if (isTextEmpty(message)) {
            //Error in typed text -> show alertDialog
            val alertDialog = AlertDialog.Builder(this)
                .apply {
                    setPositiveButton(R.string.ok) { dialog, id -> dialog.cancel() }
                    setTitle(R.string.warning_title)
                    setMessage(R.string.warning_nullStringMessage)
                }
                .create()
            alertDialog.show()
        } else {
            val intent = Intent(this, DisplayMessageActivity::class.java)

            //Text typed is ok -> delete useless spaces
            val messageToSend = cleanMessage(message)

            //Send text and start activity
            intent.putExtra(SEND_TEXT, messageToSend)
            startActivity(intent)
        }
    }

    private fun isTextEmpty(string: String): Boolean {
        return if (string.isNotEmpty()) {
            var i = 0
            var empty = true
            while (i < string.length && empty) {
                empty = string[i] == ' '
                i++
            }
            empty
        } else {
            true
        }
    }

    private fun cleanMessage(message : String):String {
        var firstCharFound = false
        var lastCharFound = false
        var firstCharPos = 0
        var lastCharPos = 0
        var i = 0
        while (i <= message.length - 1 && !firstCharFound) {
            if (message[i] != ' ') {
                firstCharFound = true
                firstCharPos = i
            }
            i++
        }
        i = message.length - 1
        while (i >= 0 && !lastCharFound) {
            if (message[i] != ' ') {
                lastCharFound = true
                lastCharPos = i
            }
            i--
        }
        val cleanedMessage = StringBuilder()
        for (j in firstCharPos..lastCharPos) {
            cleanedMessage.append(message[j])
        }
        return cleanedMessage.toString()
    }

}
