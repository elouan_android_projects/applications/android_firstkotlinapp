package com.orange.firstkotlinapp

import android.graphics.Color
import android.os.Bundle
import android.support.v4.view.GestureDetectorCompat
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.GestureDetector
import android.view.Menu
import android.view.MenuItem
import android.view.MotionEvent
import android.widget.TextView
import com.orange.firstkotlinapp.extension.*

private const val KEY_CURRENT_COLOR = "currentColor"
private const val KEY_CURRENT_SIZE = "currentSize"
private const val KEY_CURRENT_MENU = "currentMenu"

class DisplayMessageActivity : AppCompatActivity(),
    GestureDetector.OnGestureListener, ChangeFontSizeFragment.OnSizeUpdate, ChangeFontColorFragment.OnColorUpdate {

    enum class OptionMenuSelected {
        CHANGE_FONT_SIZE, CHANGE_COLOR, NONE
    }


    private val TAG = this::class.simpleName

    private lateinit var mDetector: GestureDetectorCompat
    private var mMenuSelected = OptionMenuSelected.NONE
    private val mToolbar by this.bind<Toolbar>(R.id.toolbar)
    private val mDisplayedText by bind<TextView>(R.id.DisplayMessage_displayedText)

    private var mCurrentFontColor = Color.parseColor(DEFAULT_TEXT_COLOR)
    private var mCurrentFontSize = toDensityPixel(DEFAULT_TEXT_SIZE)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_display_message)

        setSupportActionBar(mToolbar)
        // Enable the Up button
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        // Get the Intent that started this activity and extract the string
        val messageReceived = intent.getStringExtra(SEND_TEXT)

        // If there is saved saved bundle load it
        savedInstanceState?.let {
            Log.d(TAG,"Restore instance")
            mCurrentFontColor = savedInstanceState.getInt(KEY_CURRENT_COLOR)
            mCurrentFontSize = savedInstanceState.getFloat(KEY_CURRENT_SIZE)
            mMenuSelected = savedInstanceState.getSerializable(KEY_CURRENT_MENU) as OptionMenuSelected
        }

        // Set displayed text design properties
        mDisplayedText.text = messageReceived
        mDisplayedText.setTextColor(mCurrentFontColor)
        mDisplayedText.textSize = mCurrentFontSize

        mDetector = GestureDetectorCompat(this, this)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Log.d(TAG,"Saving instance")
        super.onSaveInstanceState(outState)
        outState.putInt(KEY_CURRENT_COLOR, mCurrentFontColor)
        outState.putFloat(KEY_CURRENT_SIZE, mCurrentFontSize)
        outState.putSerializable(KEY_CURRENT_MENU, mMenuSelected)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.toolbar, menu)
        return super.onCreateOptionsMenu(menu)
    }

    /**
     * MENU FUNCTIONS
     *
     */

    private fun menuChangeFontSize() {
        val bundle = Bundle()
        bundle.putFloat(SEND_FONT_SIZE_KEY, mCurrentFontSize)
        val newFragment = ChangeFontSizeFragment()
        newFragment.arguments = bundle
        val transaction = supportFragmentManager.beginTransaction()

        mMenuSelected = if (mMenuSelected != OptionMenuSelected.CHANGE_FONT_SIZE) {
            supportFragmentManager.popBackStack()
            transaction
                .setCustomAnimations(
                    R.anim.slide_in_top,
                    R.anim.slide_out_top,
                    R.anim.pop_slide_in_top,
                    R.anim.pop_slide_out_top
                )
                .replace(R.id.DisplayMessage_fragmentHolder, newFragment)
                .addToBackStack(null)
                .commit()
            OptionMenuSelected.CHANGE_FONT_SIZE
        } else {
            supportFragmentManager.popBackStack()
            OptionMenuSelected.NONE
        }
    }

    private fun menuChangeFontColor() {
        val bundle = Bundle()
        bundle.putInt(SEND_FONT_COLOR_KEY, mCurrentFontColor)
        val newFragment = ChangeFontColorFragment()
        newFragment.arguments = bundle
        val transaction = supportFragmentManager.beginTransaction()

        mMenuSelected = if (mMenuSelected != OptionMenuSelected.CHANGE_COLOR) {
            supportFragmentManager.popBackStack()
            transaction
                .setCustomAnimations(
                    R.anim.slide_in_top,
                    R.anim.slide_out_top,
                    R.anim.pop_slide_in_top,
                    R.anim.pop_slide_out_top
                )
                .replace(R.id.DisplayMessage_fragmentHolder, newFragment)
                .addToBackStack(null)
                .commit()
            OptionMenuSelected.CHANGE_COLOR
        } else {
            supportFragmentManager.popBackStack()
            OptionMenuSelected.NONE
        }
    }

    private fun menuResetToDefault() {
        mCurrentFontColor = Color.parseColor(DEFAULT_TEXT_COLOR)
        mDisplayedText.setTextColor(mCurrentFontColor)
        mCurrentFontSize = toDensityPixel(DEFAULT_TEXT_SIZE)
        mDisplayedText.textSize = mCurrentFontSize
        if (mMenuSelected != OptionMenuSelected.NONE) {
            supportFragmentManager.popBackStack()
            OptionMenuSelected.NONE
        }
    }


    override fun onOptionsItemSelected(item: MenuItem?) = when (item?.itemId) {
        R.id.action_colorChange -> {
            menuChangeFontColor()
            true
        }
        R.id.action_fontSizeChange -> {
            menuChangeFontSize()
            true
        }
        R.id.action_reset -> {
            menuResetToDefault()
            true
        }
        else -> super.onOptionsItemSelected(item)
    }

    /**
     * FRAGMENTS LISTENERS
     *
     */

    override fun updateActivitySize(newFontSize: Float) {
        mCurrentFontSize = newFontSize
        mDisplayedText.textSize = mCurrentFontSize
    }

    override fun updateActivityColor(newFontColor: Int) {
        mCurrentFontColor = newFontColor
        mDisplayedText.setTextColor(newFontColor)
    }

    /**
     * TOUCH AND MOVEMENT EVENTS
     *
     */

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (mDetector.onTouchEvent(event)) true else super.onTouchEvent(event)
    }

    override fun onScroll(event1: MotionEvent, event2: MotionEvent, distanceX: Float, distanceY: Float): Boolean {
        if (mMenuSelected != OptionMenuSelected.NONE && distanceY >= 100) {
            supportFragmentManager.popBackStack()
            OptionMenuSelected.NONE
        }
        return true
    }

    override fun onShowPress(e: MotionEvent?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onSingleTapUp(e: MotionEvent?): Boolean {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return true
    }

    override fun onDown(e: MotionEvent?): Boolean {
        //  TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return true
    }

    override fun onFling(e1: MotionEvent?, e2: MotionEvent?, velocityX: Float, velocityY: Float): Boolean {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
        return true
    }

    override fun onLongPress(e: MotionEvent?) {
        // TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

}

