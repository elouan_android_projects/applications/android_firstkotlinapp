package com.orange.firstkotlinapp

import android.content.Context
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.orange.firstkotlinapp.extension.*

private const val KEY_CURRENT_COLOR = "currentColor"

class ChangeFontColorFragment : DialogFragment() {

    private val TAG = this::class.simpleName

    private val mNextColorButton by bind<ImageButton>(R.id.ChangeColor_button_decrease)
    private val mPreviousColorButton by bind<ImageButton>(R.id.ChangeColor_button_increase)
    private val mColorTextView by bind<TextView>(R.id.ChangeColor_value)

    private var mCurrentColorId = 0

    private lateinit var mColorUpdater: OnColorUpdate

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View {
        mCurrentColorId = COLOR_LIST.indexOf(arguments!!.getInt(SEND_FONT_COLOR_KEY))
        return inflater.inflate(R.layout.fragment_change_font_color, container, false)
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putInt(KEY_CURRENT_COLOR, mCurrentColorId)
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        mColorUpdater = context as OnColorUpdate
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // If there is saved saved bundle load it
        savedInstanceState?.let { mCurrentColorId = savedInstanceState.getInt(KEY_CURRENT_COLOR) }
        updateColor(mCurrentColorId)
        Log.d(TAG, "current font color : $mCurrentColorId")
        super.onViewCreated(view, savedInstanceState)
        mNextColorButton.setOnClickListener { updateColor(mCurrentColorId.inc()) }
        mPreviousColorButton.setOnClickListener { updateColor(mCurrentColorId.dec()) }
    }


    private fun updateColor(newValue: Int) {
        mCurrentColorId = when {
            newValue > COLOR_LIST.size - 1 -> 0
            newValue < 0 -> COLOR_LIST.size - 1
            else -> newValue
        }
        mColorTextView.text = COLOR_NAME_LIST[mCurrentColorId]
        mColorUpdater.updateActivityColor(COLOR_LIST[mCurrentColorId])
        Log.d(TAG, "current font size : $mCurrentColorId")
    }

    interface OnColorUpdate {
        fun updateActivityColor(newFontColor: Int)
    }


}