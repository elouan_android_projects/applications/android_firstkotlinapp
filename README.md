## Documentation ##
Présentation de l'application et des fonctions implantés

### Présentation ###
- Auteur : Elouan PETEREAU
- Nom du projet : FirstKotlinApp
- Date de creation : 31/12/2018
- Lien Gitlab : <https://gitlab.com/elouan/android_firstkotlinapp>

### But du projet ###
Créer une première application sous Android en utilisant kotlin.

L'application permettra d'entrer un message puis de l'ouvrir dans une nouvelle page.

### Détail des fonctionnalité apportés au projet ###
- Gestion erreur de saisie (saisie vide / espace uniquement) avec message + fenêtre modal
- Suppression des espaces inutiles au début/fin de texte lors de l'envoie du message à la deuxième fenêtre
- Edition de la taille et de la couleur du texte via deux boutons dans une barre de menu (deux états possibles qui changent à chaque clic sur le bouton)
- Ouverture d'une nouvelle fenêtre pour éditer plus en profondeur la couleur et le texte (en cours)